package com.haier.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

@XStreamAlias("ROWSET")
@Data
public class HrrrsbtbMtLProductInfoList {

    @XStreamImplicit(itemFieldName="ROW")
    private List<HrrrsbtbMtLProductInfo> hrrrsbtbMtLProductInfoList;
    @XStreamAsAttribute()
    @XStreamAlias("NAME")
    private String name;
    @XStreamAsAttribute()
    @XStreamAlias("MODULE")
    private String module;

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getModule() {
//        return module;
//    }
//
//    public void setModule(String module) {
//        this.module = module;
//    }
//
//    public List<HrrrsbtbMtLProductInfo> getHrrrsbtbMtLProductInfoList() {
//        return hrrrsbtbMtLProductInfoList;
//    }
//
//    public void setHrrrsbtbMtLProductInfoList(List<HrrrsbtbMtLProductInfo> hrrrsbtbMtLProductInfoList) {
//        this.hrrrsbtbMtLProductInfoList = hrrrsbtbMtLProductInfoList;
//    }


}
