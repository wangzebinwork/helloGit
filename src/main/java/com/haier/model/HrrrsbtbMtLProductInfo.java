package com.haier.model;

import com.haier.utils.XStreamDateConverterUtils;
import com.haier.utils.XStreamNumberConverterUtils;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
@Data
@XStreamAlias("ROW")
@Table(name = "hrrrsbtb_mtl_product_info")
public class HrrrsbtbMtLProductInfo {
    @Id
    private Long id;
    @XStreamAsAttribute()
    @XStreamAlias("ROW_ID")
    private String rowId;
    @XStreamAsAttribute()
    @XStreamAlias("MATERIAL_CODE")
    private String materialCode;
    @XStreamAsAttribute()
    @XStreamAlias("MATERIAL_DESCRITION")
    private String materialDescrition;
    @XStreamAsAttribute()
    @XStreamAlias("MATERIAL_TYPE")
    private String materialType;
    @XStreamAsAttribute()
    @XStreamAlias("PRIMARY_UOM")
    private String primaryUom;
    @XStreamAsAttribute()
    @XStreamAlias("MTL_GROUP_CODE")
    private String mtlGroupCode;
    @XStreamAsAttribute()
    @XStreamAlias("DEPARTMENT")
    private String department;
    @XStreamAsAttribute()
    @XStreamAlias("WEIGHT_UOM_CODE")
    private String weightUomCode;
    @XStreamAsAttribute()
    @XStreamAlias("BASIC_MTL")
    private String basic_mtl;
    @XStreamAsAttribute()
    @XStreamAlias("PROD_LIFE_STATE")
    private String prodLifeState;
    @XStreamAsAttribute()
    @XStreamAlias("PRO_TYPE")
    private String proType;
    @XStreamAsAttribute()
    @XStreamAlias("PRO_BAND")
    private String proBand;
    @XStreamAsAttribute()
    @XStreamAlias("LENGTH_NUMBER")
//    @XStreamConverter(value=XStreamNumberConverterUtils.class)
    private Double lengthNumber;
    @XStreamAsAttribute()
    @XStreamAlias("WIDTH_NUMBER")
//    @XStreamConverter(value=XStreamNumberConverterUtils.class)
    private Double widthNumber;
    @XStreamAsAttribute()
    @XStreamAlias("HIGH_NUMBER")
//    @XStreamConverter(value=XStreamNumberConverterUtils.class)
    private Double highNumber;
    @XStreamAsAttribute()
    @XStreamAlias("GROSS_WEIGHT_NUMBER")
//    @XStreamConverter(value=XStreamNumberConverterUtils.class)
    private Double grossWeightNumber;
    @XStreamAsAttribute()
    @XStreamAlias("WEIGHT_UNIT")
    private String weightUnit;
    @XStreamAsAttribute()
    @XStreamAlias("SEND_UNIT")
    private String sendUnit;
    @XStreamAsAttribute()
    @XStreamAlias("CON_CODE")
    private String conCode;
    @XStreamAsAttribute()
    @XStreamAlias("DELETE_FLAG")
    private Character deleteFlag;
    @XStreamAsAttribute()
    @XStreamAlias("PRODUCT_LIST")
    private String productList;
    @XStreamAsAttribute()
    @XStreamAlias("SALE_FLAG")
    private Character saleFlag;
    @XStreamAsAttribute()
    @XStreamAlias("DEFAULT_PLANT_CODE")
    private String defaultPlantCode;
    @XStreamAsAttribute()
    @XStreamAlias("LAST_UPD")
    @XStreamConverter(value=XStreamDateConverterUtils.class)
    private Date lastUpd;
    @XStreamAsAttribute()
    @XStreamAlias("IS_CRUST_FLAG")
    private String isCrustFlag;
    @XStreamAsAttribute()
    @XStreamAlias("PRODUCT_LAND")
    private String productLand;
    @XStreamAsAttribute()
    @XStreamAlias("TEST_SALES_FLAG")
    private String testSalesFlag;
    @XStreamAsAttribute()
    @XStreamAlias("TEST_SALES_QTY")
    @XStreamConverter(value=XStreamNumberConverterUtils.class)
    private Long testSalesQty;
    @XStreamAsAttribute()
    @XStreamAlias("CATEGORIES_FIRST")
    private String categoriesFirst;
    @XStreamAsAttribute()
    @XStreamAlias("CATEGORIES_MID")
    private String categoriesMid;
    @XStreamAsAttribute()
    @XStreamAlias("PLATFORM")
    private String platform;
    @XStreamAsAttribute()
    @XStreamAlias("SERIES")
    private String series;
    @XStreamAsAttribute()
    @XStreamAlias("ITEM")
    private String item;
    @XStreamAsAttribute()
    @XStreamAlias("PRODUCT_TYPE")
    private String productType;
    @XStreamAsAttribute()
    @XStreamAlias("DEMONSTRATOR")
    private String demonstrator;
    @XStreamAsAttribute()
    @XStreamAlias("HIGH_END_PRODUCTS")
    private String highEndProducts;
    @XStreamAsAttribute()
    @XStreamAlias("PRODUCT_LINE_CODE")
    private String productLineCode;
    @XStreamAsAttribute()
    @XStreamAlias("CO_PRODUCT")
    private String coProduct;
    @XStreamAsAttribute()
    @XStreamAlias("DP_DATE_PLAN")
    private String dpDatePlan;
    @XStreamAsAttribute()
    @XStreamAlias("DP_DATE_ACTUAL")
    private String dpDateActual;
    @XStreamAsAttribute()
    @XStreamAlias("PA_DATE_PLAN")
    private String paDatePlan;
    @XStreamAsAttribute()
    @XStreamAlias("PA_DATE_ACTUAL")
    private String paDateActual;
    @XStreamAsAttribute()
    @XStreamAlias("PE_DATE_PLAN")
    private String peDatePlan;
    @XStreamAsAttribute()
    @XStreamAlias("PE_DATE_ACTUAL")
    private String peDateActual;
    @XStreamAsAttribute()
    @XStreamAlias("EM_DATE_PLAN")
    private String emDatePlan;
    @XStreamAsAttribute()
    @XStreamAlias("EM_DATE_ACTUAL")
    private String emDateActual;
    @XStreamAsAttribute()
    @XStreamAlias("ES_DATE_PLAN")
    private String esDatePlan;
    @XStreamAsAttribute()
    @XStreamAlias("ES_DATE_ACTUAL")
    private String esDateActual;
    @XStreamAsAttribute()
    @XStreamAlias("HR_INDEX")
    private String hrIndex;

}
