package com.haier.schedule;

import com.haier.common.ConstantMda;
import com.haier.model.HrrrsbtbMtLProductInfo;
import com.haier.model.HrrrsbtbMtLProductInfoList;
import com.haier.service.HrrrsbtbMtLProductInfoService;
import com.haier.utils.*;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
public class MdmSchedule {

    protected static final Logger logger = LoggerFactory.getLogger(MdmSchedule.class);

    @Autowired
    private HrrrsbtbMtLProductInfoService hrrrsbtbMtLProductInfoService;

    //设置定时器每30分钟查询一次接口
//    @Scheduled(fixedRate = 1000*10)
    public void fixedRate( )throws Exception {
        //接口参数
        Long beforeTime = System.currentTimeMillis() - 1000*60*30;
        String inStartDate = DateUtils.dateToFormatString(new Date(beforeTime));//查询开始时间
        String inEndDate = DateUtils.dateToFormatString(new Date());//查询结束时间
        logger.info("\n=======  开始调用接口  ========\n" + "接口参数查询开始时间:" + inStartDate+"   " + "接口参数查询结束时间：" + inEndDate);
        String inPage = "1";//查询初始页
        String inBatchId = "";
        //调用webservice
        Map<String, String> response = CallWebsercieMdmUtil.callWebsercieMdm(inStartDate, inEndDate, inPage, inBatchId);
        logger.info("\n=========  调用接口返回值  =========\n" + response);
        //xml报文解析;获取processResponse节点数据并存入对象,获取所有页数
        if (!CommonUtils.isEmptyMap(response)) {
            String outRetCode = response.get("outRetCode");
            String outResult = response.get("outResult");
            String outAllCount = response.get("outAllCount");

            //获取接口返回状态 ,判断outRetCode状态是不是“S”
            if(!CommonUtils.stringEquals(outRetCode,ConstantMda.OUT_RETCODE)
                    || CommonUtils.isEmptyStr(outResult)) return;
                //将数据存入数据库
            saveHrrrsbtbMtLProductInfo(outResult);
            if (!CommonUtils.isEmptyStr(outAllCount)) {
                Integer allCount = Integer.getInteger(outAllCount);
                if (allCount != null && allCount > 1) {
                    for (int i = 2; i < allCount+1; i++) {

                        Map<String, String> responseCurrent = CallWebsercieMdmUtil.callWebsercieMdm(inStartDate, inEndDate, String.valueOf(i), inBatchId);

                        if (!CommonUtils.isEmptyMap(response)) {
                            String outRetCodeCur = response.get("outRetCode");
                            String outResultCur = response.get("outResult");
                            //获取接口返回状态 ,判断outRetCode状态是不是“S”
                            if(!CommonUtils.stringEquals(outRetCode,ConstantMda.OUT_RETCODE)
                                    || CommonUtils.isEmptyStr(outResult)) {
                                //将数据存入数据库
                                saveHrrrsbtbMtLProductInfo(outResult);
                            }
                        }
                    }
                }
            }
        }
    }

    public void saveHrrrsbtbMtLProductInfo(String outResult) throws Exception {
        Document outResultDoc = DocumentHelper.parseText(outResult);//获取xml文件
        if(!CommonUtils.isExistEmptyObj(outResultDoc)){

            Element OUTPUT = outResultDoc.getRootElement();//获取根目录
            if(CommonUtils.isExistEmptyObj(outResult)) return ;
            Element ROWSET = OUTPUT.element("ROWSET");
            if(CommonUtils.isExistEmptyObj(outResult)) return ;
            String rowSetStr =  ROWSET.asXML();
            if(CommonUtils.isEmptyStr(rowSetStr)) return ;

            HrrrsbtbMtLProductInfoList hrrrsbtbMtLProductInfoList =(HrrrsbtbMtLProductInfoList)XstreamUtils.toBean( HrrrsbtbMtLProductInfoList.class, rowSetStr);
            //将获取的数据存入数据库
            for (HrrrsbtbMtLProductInfo hrrrsbtbMtLProductInfo : hrrrsbtbMtLProductInfoList.getHrrrsbtbMtLProductInfoList()) {

                hrrrsbtbMtLProductInfoService.insertHrrrsbtbMtLProductInfo(hrrrsbtbMtLProductInfo);
            }
        }
    }

}
