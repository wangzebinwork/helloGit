package com.haier.schedule;

import com.haier.common.ConstantMda;
import com.haier.model.HrrrsbtbMtLProductInfo;
import com.haier.model.HrrrsbtbMtLProductInfoList;
import com.haier.service.HrrrsbtbMtLProductInfoService;
import com.haier.utils.*;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class MdmScheduleWebService {

    protected static final Logger logger = LoggerFactory.getLogger(MdmScheduleWebService.class);

    @Autowired
    private HrrrsbtbMtLProductInfoService hrrrsbtbMtLProductInfoService;

    //设置定时器每30分钟查询一次接口
    @Scheduled(fixedRate = 1000*10)
    public void fixedRate( )throws Exception {
        //接口参数
        Long beforeTime = System.currentTimeMillis() - 1000*60*30;
        String inStartDate = DateUtils.dateToFormatString(new Date(beforeTime));//查询开始时间
        String inEndDate = DateUtils.dateToFormatString(new Date());//查询结束时间
        logger.info("\n=======  开始调用接口  ========\n" + "接口参数查询开始时间:" + inStartDate+"   " + "接口参数查询结束时间：" + inEndDate);
        String inPage = "1";//查询初始页
        String inBatchId = "";
        //调用webservice

        Map<String, Object> paramMap = getParamMap(inStartDate,inEndDate,inPage,inBatchId);
        String response = CallWebServiceUtils.callWebServiceAxis2Text(ConstantMda.RRSBTB_OUT_WS_URL,
                ConstantMda.NAME_SPACE, ConstantMda.METHOD_NAME, paramMap);
        logger.info("\n=========  调用接口返回值  =========\n" + response);
        getProcessResponseAndSave(response);
        //xml报文解析;获取processResponse节点数据并存入对象,获取所有页数
        System.out.println("git demo");
    }
    public String getProcessResponseAndSave(String response) throws Exception {
        Document doc = DocumentHelper.parseText(response);//获取xml文件
        if(CommonUtils.isExistEmptyObj(doc)) return null;
        Element processResponse = doc.getRootElement();//获取根目录
        if(CommonUtils.isExistEmptyObj(processResponse)) return null;
        //获取所有页数
        String outAllCount = processResponse.elementText("OUT_ALL_COUNT");
        //获取接口返回状态 ,当状态不是S的时候直接返回
        String outRetCode = processResponse.elementText("OUT_RETCODE");
        if(!CommonUtils.stringEquals(outRetCode,ConstantMda.OUT_RETCODE)) return outAllCount;
        Element outResult = processResponse.element("OUT_RESULT");
        if(CommonUtils.isExistEmptyObj(outResult)) return outAllCount;
        String outResultStr = outResult.getText();
        if(CommonUtils.isEmptyStr(outResultStr)) return outAllCount;
        Document outResultDoc = DocumentHelper.parseText(outResultStr);//获取xml文件
        if(CommonUtils.isExistEmptyObj(outResult)) return outAllCount;
        Element OUTPUT = outResultDoc.getRootElement();//获取根目录
        if(CommonUtils.isExistEmptyObj(outResult)) return outAllCount;
        Element ROWSET = OUTPUT.element("ROWSET");
        if(CommonUtils.isExistEmptyObj(outResult)) return outAllCount;
        String rowSetStr =  ROWSET.asXML();
        if(CommonUtils.isEmptyStr(outResultStr)) return outAllCount;

        HrrrsbtbMtLProductInfoList hrrrsbtbMtLProductInfoList =(HrrrsbtbMtLProductInfoList)XstreamUtils.toBean( HrrrsbtbMtLProductInfoList.class, rowSetStr);
        //将获取的数据存入数据库
        for (HrrrsbtbMtLProductInfo hrrrsbtbMtLProductInfo : hrrrsbtbMtLProductInfoList.getHrrrsbtbMtLProductInfoList()) {

            hrrrsbtbMtLProductInfoService.insertHrrrsbtbMtLProductInfo(hrrrsbtbMtLProductInfo);
        }
        return outAllCount;
    }

    // 获取XML请求报文
    public Map<String, Object>  getParamMap(String inStartDate, String inEndDate, String inPage, String inBatchId) {

        Map<String, Object> paramMap = new HashMap();
        paramMap.put("IN_SYS_NAME", ConstantMda.IN_SYS_NAME);
        paramMap.put("IN_MASTER_TYPE", ConstantMda.IN_MASTER_TYPE);
        paramMap.put("IN_TABLE_NAME", ConstantMda.IN_TABLE_NAME);
        paramMap.put("IN_STARTDATE", inStartDate);
        paramMap.put("IN_ENDDATE", inEndDate);
        paramMap.put("IN_PAGE", inPage);
        paramMap.put("IN_BATCH_ID", inBatchId);
        return paramMap;
    }
//adasdsadsadsadas
}
