
package com.haier.webservice;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "generalmdmdatarelease_client_ep", targetNamespace = "http://xmlns.oracle.com/Interface/GeneralMDMDataRelease/GeneralMDMDataRelease", wsdlLocation = "http://bpel.mdm.haier.com:7778/soa-infra/services/interface/GeneralMDMDataRelease/generalmdmdatarelease_client_ep?WSDL")
public class GeneralmdmdatareleaseClientEp
    extends Service
{

    private final static URL GENERALMDMDATARELEASECLIENTEP_WSDL_LOCATION;
    private final static WebServiceException GENERALMDMDATARELEASECLIENTEP_EXCEPTION;
    private final static QName GENERALMDMDATARELEASECLIENTEP_QNAME = new QName("http://xmlns.oracle.com/Interface/GeneralMDMDataRelease/GeneralMDMDataRelease", "generalmdmdatarelease_client_ep");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://bpel.mdm.haier.com:7778/soa-infra/services/interface/GeneralMDMDataRelease/generalmdmdatarelease_client_ep?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        GENERALMDMDATARELEASECLIENTEP_WSDL_LOCATION = url;
        GENERALMDMDATARELEASECLIENTEP_EXCEPTION = e;
    }

    public GeneralmdmdatareleaseClientEp() {
        super(__getWsdlLocation(), GENERALMDMDATARELEASECLIENTEP_QNAME);
    }

    public GeneralmdmdatareleaseClientEp(WebServiceFeature... features) {
        super(__getWsdlLocation(), GENERALMDMDATARELEASECLIENTEP_QNAME, features);
    }

    public GeneralmdmdatareleaseClientEp(URL wsdlLocation) {
        super(wsdlLocation, GENERALMDMDATARELEASECLIENTEP_QNAME);
    }

    public GeneralmdmdatareleaseClientEp(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, GENERALMDMDATARELEASECLIENTEP_QNAME, features);
    }

    public GeneralmdmdatareleaseClientEp(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public GeneralmdmdatareleaseClientEp(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns GeneralMDMDataRelease
     */
    @WebEndpoint(name = "GeneralMDMDataRelease_pt")
    public GeneralMDMDataRelease getGeneralMDMDataReleasePt() {
        return super.getPort(new QName("http://xmlns.oracle.com/Interface/GeneralMDMDataRelease/GeneralMDMDataRelease", "GeneralMDMDataRelease_pt"), GeneralMDMDataRelease.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GeneralMDMDataRelease
     */
    @WebEndpoint(name = "GeneralMDMDataRelease_pt")
    public GeneralMDMDataRelease getGeneralMDMDataReleasePt(WebServiceFeature... features) {
        return super.getPort(new QName("http://xmlns.oracle.com/Interface/GeneralMDMDataRelease/GeneralMDMDataRelease", "GeneralMDMDataRelease_pt"), GeneralMDMDataRelease.class, features);
    }

    private static URL __getWsdlLocation() {
        if (GENERALMDMDATARELEASECLIENTEP_EXCEPTION!= null) {
            throw GENERALMDMDATARELEASECLIENTEP_EXCEPTION;
        }
        return GENERALMDMDATARELEASECLIENTEP_WSDL_LOCATION;
    }

}
