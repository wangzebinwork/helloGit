
package com.haier.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IN_SYS_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IN_MASTER_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IN_TABLE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IN_STARTDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IN_ENDDATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IN_PAGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IN_BATCH_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "insysname",
    "inmastertype",
    "intablename",
    "instartdate",
    "inenddate",
    "inpage",
    "inbatchid"
})
@XmlRootElement(name = "process")
public class Process {

    @XmlElement(name = "IN_SYS_NAME", required = true)
    protected String insysname;
    @XmlElement(name = "IN_MASTER_TYPE", required = true)
    protected String inmastertype;
    @XmlElement(name = "IN_TABLE_NAME", required = true)
    protected String intablename;
    @XmlElement(name = "IN_STARTDATE", required = true)
    protected String instartdate;
    @XmlElement(name = "IN_ENDDATE", required = true)
    protected String inenddate;
    @XmlElement(name = "IN_PAGE", required = true)
    protected String inpage;
    @XmlElement(name = "IN_BATCH_ID", required = true)
    protected String inbatchid;

    /**
     * ��ȡinsysname���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSYSNAME() {
        return insysname;
    }

    /**
     * ����insysname���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSYSNAME(String value) {
        this.insysname = value;
    }

    /**
     * ��ȡinmastertype���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINMASTERTYPE() {
        return inmastertype;
    }

    /**
     * ����inmastertype���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINMASTERTYPE(String value) {
        this.inmastertype = value;
    }

    /**
     * ��ȡintablename���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINTABLENAME() {
        return intablename;
    }

    /**
     * ����intablename���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINTABLENAME(String value) {
        this.intablename = value;
    }

    /**
     * ��ȡinstartdate���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTARTDATE() {
        return instartdate;
    }

    /**
     * ����instartdate���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTARTDATE(String value) {
        this.instartdate = value;
    }

    /**
     * ��ȡinenddate���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINENDDATE() {
        return inenddate;
    }

    /**
     * ����inenddate���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINENDDATE(String value) {
        this.inenddate = value;
    }

    /**
     * ��ȡinpage���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINPAGE() {
        return inpage;
    }

    /**
     * ����inpage���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINPAGE(String value) {
        this.inpage = value;
    }

    /**
     * ��ȡinbatchid���Ե�ֵ��
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINBATCHID() {
        return inbatchid;
    }

    /**
     * ����inbatchid���Ե�ֵ��
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINBATCHID(String value) {
        this.inbatchid = value;
    }

}
