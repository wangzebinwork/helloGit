package com.haier.dao;

import com.haier.model.HrrrsbtbMtLProductInfo;
import tk.mybatis.mapper.common.Mapper;

public interface HrrrsbtbMtLProductInfoDao extends Mapper<HrrrsbtbMtLProductInfo> {

}
