package com.haier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.haier.dao")
@EnableScheduling
public class HaierMdmApplication {

	public static void main(String[] args) {
		SpringApplication.run(HaierMdmApplication.class, args);
	}
}
