package com.haier.common;

public class ConstantMda {

    //接口RRSBTB_OUT_WS的地址
    public static final String RRSBTB_OUT_WS_URL = new String("http://bpel.mdm.haier.com:7778/soa-infra/services/interface/GeneralMDMDataRelease/generalmdmdatarelease_client_ep?WSDL");
    //系统编号
    public static final String IN_SYS_NAME = new String( "S00073" ) ;
    //模块标识
    public static final String IN_MASTER_TYPE = new String( "HAIERMDM" ) ;
    //MDM表名
    public static final String IN_TABLE_NAME = new String( "HRRRSBTB_MTL_PRODUCT_INFO" );

    public static final String NAME_SPACE = new String( "http://xmlns.oracle.com/Interface/GeneralMDMDataRelease/GeneralMDMDataRelease");
    public static final String METHOD_NAME = new String( "process" );
    public static final String OUT_RETCODE = new String( "S" );

}
