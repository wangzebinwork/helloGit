package com.haier.utils;

import java.util.List;
import java.util.Map;

public class CommonUtils {

    /**
     * 判断某个list是否为空
     * @param <T>
     * @param list
     * @return
     */
    public static <T> boolean isEmptyList(List<T> list) {
        return list == null || list.isEmpty();
    }

    /**
     * 判断某个map是否为空
     * @param <K>
     * @param <V>
     * @param map
     * @return
     */
    public static <K, V> boolean isEmptyMap(Map<K, V> map) {
        return map == null || map.isEmpty();
    }

    /**
     * 判断某字符串是否为空
     * @param str
     * @return
     */
    public static boolean isEmptyStr(String str) {
        return str == null || "".equals(str);
    }

    //只有不传入参，objs才会是空,这个判断是给下面循环判空用的
    public static boolean isExistEmptyObj(Object... objs) {
        if (objs == null) {
            return false;
        }

        for (Object obj : objs) {
            if (obj == null || "".equals(getString(obj))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 将某业务对象转化成String类型，一般用于从map中取值
     *
     * @param object
     * @return
     */
    public static String getString(Object object) {
        return object == null ? null : object.toString();
    }

    /**
     * 判断String对象值是否相等。两个对象都为null时返回true。
     * @param str1
     * @param str2
     * @return
     */
    public static boolean stringEquals(String str1,String str2){
        if (null == str1 && null == str2) {
            return true;
        }
        if (null != str1) {
            if (str1.equals(str2)) {
                return true;
            } else {
                return false;
            }
        }
        if (null != str2) {
            if (str2.equals(str1)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 判断Long对象值是否相等。两个对象都为null时返回true。
     * @param long1
     * @param long2
     * @return
     */
    public static boolean longEquals(Long long1, Long long2) {
        if (null == long1 && null == long2) {
            return true;
        }
        if (null != long1) {
            if (long1.equals(long2)) {
                return true;
            } else {
                return false;
            }
        }
        if (null != long2) {
            if (long2.equals(long1)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 判断Integer对象值是否相等。两个对象都为null时返回true。
     * @param integer1
     * @param integer2
     * @return
     */
    public static boolean integerEquals(Integer integer1, Integer integer2) {
        if (null == integer1 && null == integer2) {
            return true;
        }
        if (null != integer1) {
            if (integer1.equals(integer2)) {
                return true;
            } else {
                return false;
            }
        }
        if (null != integer2) {
            if (integer2.equals(integer1)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
