package com.haier.utils;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;

import java.util.Map;


/**
 * call调用webservice接口用具类
 * Created by AlterEgo
 */
public class CallWebServiceUtils {

    /**
     * call调用webservice接口
     *
     * @param url        接口地址
     * @param nameSpace  命名空间
     * @param methodName 方法名
     * @param paramMap   参数Map
     * @return
     */
    public static OMElement callWebServiceAxis2(String url, String nameSpace, String methodName, Map<String, Object> paramMap) throws Exception {
        OMElement result = null;
        try {
            ServiceClient serviceClient = new ServiceClient();
            //创建WebService的URL
            EndpointReference targetEPR = new EndpointReference(url);
            Options options = serviceClient.getOptions();
            options.setTo(targetEPR);
            options.setTimeOutInMilliSeconds(10000);
            options.setManageSession(true);
            options.setProperty(HTTPConstants.REUSE_HTTP_CLIENT,true);
            serviceClient.cleanupTransport();

            //确定调用方法（ 命名空间地址 (namespace) 和 方法名称）
            options.setAction(nameSpace);
            OMFactory fac = OMAbstractFactory.getOMFactory();
            OMNamespace omNs = fac.createOMNamespace(nameSpace,"");
            OMElement method = fac.createOMElement(methodName, omNs);
            // 遍历传入方法的参数
            for (String key : paramMap.keySet()) {
                OMElement element = fac.createOMElement(key, omNs);
                Object obj = paramMap.get(key);
                if (obj != null) {
                    element.setText(obj.toString());
                }
                method.addChild(element);
            }
            method.build();
            //调用接口
            result = serviceClient.sendReceive(method);
        } catch (Exception ex) {
            throw new RuntimeException("调用接口\"RRSBTB_OUT_WSRRSBTB_OUT_WS\"发生异常:\n"+ex.getMessage());
        }
        return result;
    }

    /**
     * call调用webservice接口
     * @param url        接口地址
     * @param nameSpace  命名空间
     * @param methodName 方法名
     * @param paramMap   参数Map
     * @return
     */
    public static String callWebServiceAxis2Text(String url, String nameSpace, String methodName, Map<String, Object> paramMap) throws Exception {
        OMElement element = callWebServiceAxis2(url, nameSpace, methodName, paramMap);
        String resultTexe = "";
        if (element != null) {
            String text = element.getText();
            resultTexe = element.toString();

        }
        return resultTexe;
    }

//    public void run() {
//        QueryCreditBalanceOfCustomers_Service service = getCreditBalanceOfCustomersService();
//        QueryCreditBalanceOfCustomers b2BCreditBalanceServie = service.getQueryCreditBalanceOfCustomersSOAP();
//        List<OMSOUT> omsOUTList = new ArrayList<OMSOUT>();
//        OMSOUT omsout = new OMSOUT();
//        omsout.setKUNNR(kunnr);
//        omsout.setKKBER(kkber);
//        omsOUTList.add(omsout);
//        EAIInputMessage eaiInfo = new EAIInputMessage();
//        long startTime = System.nanoTime() / 1000 / 1000;
//        log.info("welinksoft-CreditBalanceStart:" + "kunnr----------" + kunnr + ":kkber----------" + kkber + "startTime:" + startTime);
//        b2BCreditBalanceServie.queryCreditBalanceOfCustomers(omsOUTList, eaiInfo, out, eaiout);
//        long endTime = System.nanoTime() / 1000 / 1000;
//        long result = endTime - startTime;
//    }



//    public QueryCreditBalanceOfCustomers_Service getCreditBalanceOfCustomersService() {
//        if (creditBalanceOfCustomersService == null) {
//            URL url = ResourceUtil.getResourceUrl(balance365WsdlUrl);
//            creditBalanceOfCustomersService = new QueryCreditBalanceOfCustomers_Service(url);
//        }
//        return creditBalanceOfCustomersService;
//    }
}
