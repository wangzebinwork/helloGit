package com.haier.utils;

import com.haier.common.ConstantMda;
import com.haier.webservice.GeneralMDMDataRelease;
import com.haier.webservice.GeneralmdmdatareleaseClientEp;

import javax.xml.ws.Holder;
import java.util.HashMap;
import java.util.Map;

public class CallWebsercieMdmUtil {

    public static Map<String,String> callWebsercieMdm(String inStartDate, String inEndDate, String inPage, String inBatchId) {

        GeneralmdmdatareleaseClientEp generalmdmdatareleaseClientEp = new GeneralmdmdatareleaseClientEp();
        Holder<String> outPAGE = new Holder<String>();
        Holder<String> outRESULT = new Holder<String>();
        Holder<String> outRETCODE = new Holder<String>();
        Holder<String> outALLNUM = new Holder<String>();
        Holder<String> outPAGECON = new Holder<String>();
        Holder<String> outALLCOUNT = new Holder<String>();
        Holder<String> outRETMSG = new Holder<String>();
        Holder<String> outBATCHID = new Holder<String>();

        GeneralMDMDataRelease generalMDMDataRelease = generalmdmdatareleaseClientEp.getGeneralMDMDataReleasePt();
        generalMDMDataRelease.process(ConstantMda.IN_SYS_NAME, ConstantMda.IN_MASTER_TYPE,ConstantMda.IN_TABLE_NAME,
                                        inStartDate,inEndDate,inPage,inBatchId,
                                        outPAGE,outRESULT,outRETCODE,outALLNUM,
                                        outPAGECON,outALLCOUNT,outRETMSG,outBATCHID);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("outAllCount",outALLCOUNT.value);
        hashMap.put("outResult",outRESULT.value);
        hashMap.put("outRetCode",outRETCODE.value);
        return hashMap;
    }
}
