package com.haier.utils;


import com.thoughtworks.xstream.converters.SingleValueConverter;
public class XStreamNumberConverterUtils implements SingleValueConverter {

    @Override
    public boolean canConvert(Class type) {
        return type.equals(Long.class);
    }

    @Override
    public Object fromString(String str) {
        // 这里将字符串转换成日期
        if (CommonUtils.isEmptyStr(str.trim())) {
            return null;
        }
        return Long.valueOf(str);
    }

    @Override
    public String toString(Object obj) {
        // 这里将日期转换成字符串
        return obj.toString();
    }

}
