package com.haier.utils;

import com.thoughtworks.xstream.XStream;

public class XstreamUtils {

    public static Object toBean(Class<?> clazz, String xml) {
        Object xmlObject = null;
        XStream xstream = new XStream();
        xstream.processAnnotations(clazz);
        xstream.autodetectAnnotations(true);
        xstream.setClassLoader(clazz.getClassLoader());
        xmlObject= xstream.fromXML(xml);
        return xmlObject;
    }
}
