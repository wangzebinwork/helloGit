package com.haier.service.impl;

import com.haier.dao.HrrrsbtbMtLProductInfoDao;
import com.haier.model.HrrrsbtbMtLProductInfo;
import com.haier.service.HrrrsbtbMtLProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class HrrrsbtbMtLProductInfoServiceImpl implements HrrrsbtbMtLProductInfoService {

    @Autowired
    private HrrrsbtbMtLProductInfoDao hrrrsbtbMtLProductInfoDao;

    @Override
    public void insertHrrrsbtbMtLProductInfo(HrrrsbtbMtLProductInfo hrrrsbtbMtLProductInfo) {
        int insert = hrrrsbtbMtLProductInfoDao.insert(hrrrsbtbMtLProductInfo);
    }
}
